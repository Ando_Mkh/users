'use strict';

module.exports = {
  errorMessages: {
    tokenDoesntMatch: 'Token doesn\'t match userID param',
    userNotFound: 'User is not found',
    noFile: 'Can\'t open file'
  },
  internalMessages: {
    successfullyAdded: 'Successfully added',
    successfullyUpdated: 'Successfully updated'
  }
};
