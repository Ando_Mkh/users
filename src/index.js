'use strict';

/**
 * Module Dependencies
 */

const express = require('express');
const bodyParser = require('body-parser');
const logger = require('morgan');
const helmet = require('helmet');
const log = require('./utils/logger');
const config = require('./config');
const app = express();
const cors = require('cors');
const routes = require('./routes');
const { checkHttpOptions } = require('./middlewares');
// const jwt = require('express-jwt');
/**
 * Welcome message
 */

app.get('/', (req, res) => res.send('Welcome to node-express-boilerplate API server'));

/**
 * Connecting db
 */

// require('./db/connection');

/**
 * Other Middleware
 */

app.disable('x-powered-by');
app.use(checkHttpOptions);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(logger('dev'));
app.use(helmet());
app.use(cors());
// app.use(jwt({secret: config.jwtSecret}).unless({path: [new RegExp('/api/' + config.access.publicRoutes.join('|'), 'ig')]}));
app.use('/api', /* jwt({secret: config.jwtSecret}).unless({path: [new RegExp('/api/' + config.access.publicRoutes.join('|'), 'ig')]}), */routes);

/**
 * Middleware For Not Found
 */

app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

/**
 * Production error handler
 */

app.use((err, req, res, next) => {
  log.error(err);
  if (err && err.error && err.error.isJoi) {
    return res.status(400).json({
      errors: [{
        msg: err.error.toString()
      }]
    });
  }
  return res.status(err.status || 500).json({
    errors: [{
      msg: err.message
    }]
  });
});

/**
 * Application listening on PORT
 */

app.listen(config.port, log.info(`Node.js server is running at http://localhost:${config.port} in ${config.nodeEnv} mode with process id ${process.pid}`));

/**
 * Checking Uncaught Exceptions
 */

process.on('uncaughtException', err => {
  log.error(new Date().toUTCString() + ' uncaughtException:', err.message);
  log.error(err.stack);
  process.exit(1);
});

/**
 * Checking Unhandled Rejection
 */

process.on('unhandledRejection', err => {
  log.error(new Date().toUTCString() + ' unhandledRejection:', err.message);
  log.error(err.stack);
  process.exit(1);
});
