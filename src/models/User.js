const {Sequelize} = require('sequelize');
const databaseService = require('../db/connection');

const User = databaseService.define('User', {
  firstName: {type: Sequelize.STRING, allowNull: false, required: true},
  lastName: {type: Sequelize.STRING, allowNull: false, required: true},
  email: {type: Sequelize.STRING, allowNull: false, required: true},
  password: {type: Sequelize.STRING, allowNull: false, required: true}
}, {});

module.exports = User;
