'use strict';

const Joi = require('@hapi/joi');
module.exports = Joi.object({
  firstName: Joi.string()
    .alphanum()
    .min(3)
    .required(),
  lastName: Joi.string()
    .alphanum()
    .min(3)
    .required()
});
