'use strict';

const createUserValidator = require('./create-user');

module.exports = {
  createUserValidator
};
