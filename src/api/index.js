'use strict';

const HealthAPI = require('./health');
const UsersAPI = require('./users');

module.exports = {
  HealthAPI,
  UsersAPI
};
