'use strict';

const express = require('express');
const router = express.Router();
const {createSuccess} = require('../../utils/response');

/**
 * @api
 * @description
 */
router.get('/', async (req, res, next) => {
  try {
    return res.status(200).send(createSuccess({status: 'UP'}));
  } catch (e) {
    return res.status(500).send(createSuccess({status: 'ERRORED'}));
  }
});

module.exports = router;
