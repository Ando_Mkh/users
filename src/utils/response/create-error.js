'use-strict';

module.exports = message => {
  return {
    status: 'error',
    message
  };
};
