'use-strict';

module.exports = data => {
  return {
    status: 'success',
    data
  };
};
