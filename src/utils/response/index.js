'use strict';

const createSuccess = require('./create-success');
const createFail = require('./create-fail');
const createError = require('./create-error');

module.exports = {
  createSuccess,
  createFail,
  createError
};
