const app = require('express')();
const routes = require('./api');
app.use('/health', routes.HealthAPI);
app.use('/users', routes.UsersAPI);

/**
 * Swagger setup
 */
const swaggerUi = require('swagger-ui-express');
const path = require('path');
const YAML = require('yamljs');
const swaggerDocument = YAML.load(path.join(__dirname, '../swagger.config.yaml'));

app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument, {
  explorer: true
}));

module.exports = app;
